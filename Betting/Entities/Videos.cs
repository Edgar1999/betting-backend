﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Betting
{
    public class Videos
    {
        [Key]
        public int Idvideos { get; set; }
        public int Event_key { get; set; }
        public string Video_title_full { get; set; }
        public string Video_title { get; set; }
        public string Video_url { get; set; }
        public string File_name { get; set; }

    }
}
