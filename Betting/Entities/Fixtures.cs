﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Betting
{
    public class Fixtures
    {
        [Key]
        public int Idfixtures { get; set; }
        public int Event_key { get; set; }
        public string Event_date { get; set; }
        public string Event_time { get; set; }
        public string Event_home_team { get; set; }
        public int Home_team_key { get; set; }
        public string Event_away_team { get; set; }
        public int Away_team_key { get; set; }
        public string Event_halftime_result { get; set; }
        public string Event_final_result { get; set; }
        public string Event_ft_result { get; set; }
        public string Event_penalty_result { get; set; }
        public string Event_status { get; set; }
        public string Country_name { get; set; }
        public string League_name { get; set; }
        public int League_key { get; set; }
        public string League_round { get; set; }
        public string League_season { get; set; }
        public int Event_live { get; set; }
        public string Event_stadium { get; set; }
        public string Event_referee { get; set; }
        public string Home_team_logo { get; set; }
        public string Away_team_logo { get; set; }
        public int Event_country_key { get; set; }
    }
}
