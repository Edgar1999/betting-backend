﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Betting
{
    public class Rapid_api
    {
        [Key]
        public int Idrapid_api { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }
        public string Side1 { get; set; }
        public string Side2 { get; set; }
        public string Competition_name { get; set; }
        public int Competition_id { get; set; }
        public string Youtube_video_url { get; set; }
        public string Iframe_src { get; set; }
        public string File_name { get; set; }
    }
}
