﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Betting.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class DataController : Controller
    {
        public Context db;

        public DataController(Context context)
        {
            db = context;
        }

        [HttpGet]
        public async Task<IActionResult> GetFixtures()
        {
            var fixtures = await db.Fixtures.ToListAsync();

            return new JsonResult(fixtures);
        }

        [HttpGet]
        public async Task<IActionResult> GetRapidApi()
        {
            var rapidApi = await db.Rapid_Api.ToListAsync();
            return new JsonResult(rapidApi);
        }

        [HttpGet]
        public async Task<IActionResult> GetVideos()
        {
            var videos = await db.Videos.ToListAsync();

            return new JsonResult(videos);
        }

        [HttpGet]
        public async Task<IActionResult> GetVideosSecond()
        {
            var videosSecond = await db.Videos_Second.ToListAsync();

            return new JsonResult(videosSecond);
        }

        [Obsolete]
        [HttpGet]
        public async Task<IActionResult> FixturesTransfer()
        {
            var fixtures = await db.Fixtures.ToListAsync();

            using (SqlConnection conn = new SqlConnection())
            {
                string connectString = "Server=DESKTOP-RT1VTO5\\MSSQLSERVER02;Initial Catalog=BettingDb;MultipleActiveResultSets=True;Integrated Security=True;";
                conn.ConnectionString = connectString;
                SqlDataAdapter sqlData = new SqlDataAdapter("fixtures", conn);
                conn.Open();
                foreach (var item in fixtures)
                {
                    sqlData.InsertCommand = new SqlCommand("Insert into fixtures (idfixtures,event_key,event_date,event_time,event_home_team,home_team_key,event_away_team,away_team_key,event_halftime_result,event_final_result,event_ft_result,event_penalty_result,event_status,country_name,league_name,league_key,league_round,league_season,event_live,event_stadium,event_referee,home_team_logo,away_team_logo,event_country_key) " +
                        $"values({item.Idfixtures},{item.Event_key},'{item.Event_date}','{item.Event_time}','{item.Event_home_team}',{item.Home_team_key},'{item.Event_away_team}',{item.Away_team_key},'{item.Event_halftime_result}','{item.Event_final_result}','{item.Event_ft_result}','{item.Event_penalty_result}','{item.Event_status}','{item.Country_name}','{item.League_name}',{item.League_key},'{item.League_round}','{item.League_season}',{item.Event_live},'{item.Event_stadium.Replace("'","''")}','{item.Event_referee.Replace("'","''")}','{item.Home_team_logo}','{item.Away_team_logo}',{item.Event_country_key})", conn);
                    var mydata = sqlData.InsertCommand.ExecuteReader();
                    mydata.Close();
                }

                conn.Close();
            }

            return Ok();
        }

        [Obsolete]
        [HttpGet]
        public async Task<IActionResult> RapidApiTransfer()
        {
            var rapidApi = await db.Rapid_Api.ToListAsync();

            using (SqlConnection conn = new SqlConnection())
            {
                string connectString = "Server=DESKTOP-RT1VTO5\\MSSQLSERVER02;Initial Catalog=BettingDb;MultipleActiveResultSets=True;Integrated Security=True;";
                conn.ConnectionString = connectString;
                SqlDataAdapter sqlData = new SqlDataAdapter("rapid_api", conn);
                conn.Open();
                foreach (var item in rapidApi)
                {
                    try
                    {
                        sqlData.InsertCommand = new SqlCommand("insert into rapid_api (idrapid_api,title,url,side1,side2,competition_name,competition_id,youtube_video_url,iframe_src,file_name)" +
                            $"values({item.Idrapid_api},'{item.Title}','{item.Url}','{item.Side1}','{item.Side2}','{item.Competition_name}',{item.Competition_id},'{item.Youtube_video_url}','{item.Iframe_src}','{item.File_name}')", conn);
                        var mydata = sqlData.InsertCommand.ExecuteReader();
                        mydata.Close();
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }

                conn.Close();
            }

            return Ok();
        }

        [Obsolete]
        [HttpGet]
        public async Task<IActionResult> VideosTransfer()
        {
            var videos = await db.Videos.ToListAsync();

            using (SqlConnection conn = new SqlConnection())
            {
                string connectString = "Server=DESKTOP-RT1VTO5\\MSSQLSERVER02;Initial Catalog=BettingDb;MultipleActiveResultSets=True;Integrated Security=True;";
                conn.ConnectionString = connectString;
                SqlDataAdapter sqlData = new SqlDataAdapter("videos", conn);
                conn.Open();
                foreach (var item in videos)
                {
                    try
                    {
                        sqlData.InsertCommand = new SqlCommand("insert into videos (idvideos,event_key,video_title_full,video_title,video_url,file_name)" +
                            $"values({item.Idvideos},{item.Event_key},'{item.Video_title_full.Replace("'","''")}','{item.Video_title.Replace("'", "''")}','{item.Video_url}','{item.File_name}')", conn);
                        var mydata = sqlData.InsertCommand.ExecuteReader();
                        mydata.Close();
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }

                conn.Close();
            }

            return Ok();
        }

        [Obsolete]
        [HttpGet]
        public async Task<IActionResult> VideosSecondTransfer()
        {
            var videos_second = await db.Videos_Second.ToListAsync();

            using (SqlConnection conn = new SqlConnection())
            {
                string connectString = "Server=DESKTOP-RT1VTO5\\MSSQLSERVER02;Initial Catalog=BettingDb;MultipleActiveResultSets=True;Integrated Security=True;";
                conn.ConnectionString = connectString;
                SqlDataAdapter sqlData = new SqlDataAdapter("videos_second", conn);
                conn.Open();
                foreach (var item in videos_second)
                {
                    try
                    {
                        sqlData.InsertCommand = new SqlCommand("insert into videos_second (idvideos_second,event_key,video_title_full,video_title,video_url,file_name)" +
                            $"values({item.Idvideos_second},{item.Event_key},'{item.Video_title_full.Replace("'", "''")}','{item.Video_title.Replace("'", "''")}','{item.Video_url}','{item.File_name}')", conn);
                        var mydata = sqlData.InsertCommand.ExecuteReader();
                        mydata.Close();
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }

                conn.Close();
            }

            return Ok();
        }
    }
}
