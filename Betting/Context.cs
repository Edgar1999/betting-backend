﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Betting
{
    public class Context : DbContext
    {
        public Context(DbContextOptions<Context> options) : base(options) { }
        public DbSet<Fixtures> Fixtures { get; set; }
        public DbSet<Rapid_api> Rapid_Api { get; set; }
        public DbSet<Videos> Videos { get; set; }
        public DbSet<Videos_second> Videos_Second { get; set; }

        public Context()
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        }
    }
}
